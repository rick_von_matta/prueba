﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UTubeSordeado
{
    public partial class usrCtrlReproductorFlash : UserControl
    {
        protected override void WndProc(ref Message m)
        {
            switch (m.Msg)
            {
                case 0x84:
                    base.WndProc(ref m);
                    if ((int)m.Result == 0x1)
                        m.Result = (IntPtr)0x2;
                    return;
            }

            base.WndProc(ref m);
        }

        public usrCtrlReproductorFlash()
        {
            InitializeComponent();
        }

        private void picBxCerrar_Click(object sender, EventArgs e)
        {
            Cursor cur = this.Cursor;
            this.Cursor = Cursors.WaitCursor;

            try
            {
                ((Form)this.TopLevelControl).Close();
            }
            catch (Exception ex)
            {
                this.Cursor = cur;
                MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void picBxReproducir_Click(object sender, EventArgs e)
        {
            Cursor cur = this.Cursor;
            this.Cursor = Cursors.WaitCursor;

            try
            {
                this.flashPlayer.Stop();
                this.flashPlayer.Movie = "https://www.youtube.com/v/" + this.txtURL.Text;
                this.flashPlayer.Play();
                this.Cursor = cur;
            }
            catch (Exception ex)
            {
                this.Cursor = cur;
                MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
    }
}
