﻿namespace UTubeSordeado
{
    partial class usrCtrlReproductorFlash
    {
        /// <summary> 
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar 
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(usrCtrlReproductorFlash));
            this.picBxReproducir = new System.Windows.Forms.PictureBox();
            this.picBxCerrar = new System.Windows.Forms.PictureBox();
            this.txtURL = new System.Windows.Forms.TextBox();
            this.flashPlayer = new AxShockwaveFlashObjects.AxShockwaveFlash();
            ((System.ComponentModel.ISupportInitialize)(this.picBxReproducir)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBxCerrar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.flashPlayer)).BeginInit();
            this.SuspendLayout();
            // 
            // picBxReproducir
            // 
            this.picBxReproducir.Image = ((System.Drawing.Image)(resources.GetObject("picBxReproducir.Image")));
            this.picBxReproducir.Location = new System.Drawing.Point(167, 5);
            this.picBxReproducir.Name = "picBxReproducir";
            this.picBxReproducir.Size = new System.Drawing.Size(23, 21);
            this.picBxReproducir.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picBxReproducir.TabIndex = 7;
            this.picBxReproducir.TabStop = false;
            this.picBxReproducir.Click += new System.EventHandler(this.picBxReproducir_Click);
            // 
            // picBxCerrar
            // 
            this.picBxCerrar.Image = ((System.Drawing.Image)(resources.GetObject("picBxCerrar.Image")));
            this.picBxCerrar.Location = new System.Drawing.Point(191, 5);
            this.picBxCerrar.Name = "picBxCerrar";
            this.picBxCerrar.Size = new System.Drawing.Size(23, 21);
            this.picBxCerrar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picBxCerrar.TabIndex = 6;
            this.picBxCerrar.TabStop = false;
            this.picBxCerrar.Click += new System.EventHandler(this.picBxCerrar_Click);
            // 
            // txtURL
            // 
            this.txtURL.Location = new System.Drawing.Point(0, 5);
            this.txtURL.Name = "txtURL";
            this.txtURL.Size = new System.Drawing.Size(161, 20);
            this.txtURL.TabIndex = 5;
            // 
            // flashPlayer
            // 
            this.flashPlayer.Enabled = true;
            this.flashPlayer.Location = new System.Drawing.Point(0, 27);
            this.flashPlayer.Name = "flashPlayer";
            this.flashPlayer.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("flashPlayer.OcxState")));
            this.flashPlayer.Size = new System.Drawing.Size(217, 196);
            this.flashPlayer.TabIndex = 4;
            // 
            // usrCtrlReproductorFlash
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.picBxReproducir);
            this.Controls.Add(this.picBxCerrar);
            this.Controls.Add(this.txtURL);
            this.Controls.Add(this.flashPlayer);
            this.Name = "usrCtrlReproductorFlash";
            this.Size = new System.Drawing.Size(217, 226);
            ((System.ComponentModel.ISupportInitialize)(this.picBxReproducir)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBxCerrar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.flashPlayer)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox picBxReproducir;
        private System.Windows.Forms.PictureBox picBxCerrar;
        private System.Windows.Forms.TextBox txtURL;
        private AxShockwaveFlashObjects.AxShockwaveFlash flashPlayer;
    }
}
