﻿namespace UTubeSordeado
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.usrCtrlReproductorFlash1 = new UTubeSordeado.usrCtrlReproductorFlash();
            this.SuspendLayout();
            // 
            // usrCtrlReproductorFlash1
            // 
            this.usrCtrlReproductorFlash1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.usrCtrlReproductorFlash1.Location = new System.Drawing.Point(0, 0);
            this.usrCtrlReproductorFlash1.Name = "usrCtrlReproductorFlash1";
            this.usrCtrlReproductorFlash1.Size = new System.Drawing.Size(219, 226);
            this.usrCtrlReproductorFlash1.TabIndex = 0;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlDark;
            this.ClientSize = new System.Drawing.Size(219, 226);
            this.ControlBox = false;
            this.Controls.Add(this.usrCtrlReproductorFlash1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Form1";
            this.ShowIcon = false;
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private usrCtrlReproductorFlash usrCtrlReproductorFlash1;

    }
}

